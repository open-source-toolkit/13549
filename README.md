# TurtleBot3 深度强化学习避障

## 项目描述

本项目提供了一个基于PyTorch的TurtleBot3深度强化学习避障解决方案。项目中包含了多种强化学习算法，如DQN、DDPG、PPO和SAC，用于实现TurtleBot3在复杂环境中的自主避障功能。

## 主要内容

- **DQN (Deep Q-Network)**: 深度Q网络，适用于离散动作空间。
- **DDPG (Deep Deterministic Policy Gradient)**: 深度确定性策略梯度，适用于连续动作空间。
- **PPO (Proximal Policy Optimization)**: 近端策略优化，一种高效的策略梯度方法。
- **SAC (Soft Actor-Critic)**: 软演员-评论家，结合了最大熵强化学习和确定性策略梯度。

## 使用说明

1. **环境配置**:
   - 确保你已经安装了ROS (Robot Operating System) 和PyTorch。
   - 克隆本仓库到你的本地环境。

2. **代码修改**:
   - 根据你的实际路径修改代码中的路径配置。
   - 确保神经网络的输入维度与雷达接收到的数据维度一致。
   - 调整存储空间的大小，使其为输入数据大小的两倍加上奖励数据和动作数据的大小。

3. **运行代码**:
   - 根据需要选择合适的强化学习算法进行训练和测试。
   - 代码可用于其他环境和雷达小车，只需进行相应的参数调整。

## 注意事项

- 本项目中的代码是基于[Crawford-fang/ROS_pytorch_RL](https://github.com/Crawford-fang/ROS_pytorch_RL/tree/main/DQN)的改进版本，原作者的代码中存在一些需要修改的地方，如路径配置、神经网络输入维度和存储空间大小等。
- 建议在使用前仔细阅读代码，并根据实际情况进行调整。

## 贡献

欢迎大家提交问题、建议和改进代码的PR。让我们一起完善这个项目，使其在更多场景中发挥作用！

## 许可证

本项目采用MIT许可证，详情请参阅[LICENSE](LICENSE)文件。